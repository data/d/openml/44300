# OpenML dataset: Meta_Album_RSICB_Mini

https://www.openml.org/d/44300

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album RSICB Dataset (Mini)**
***
RSICB128 dataset (https://github.com/lehaifeng/RSI-CB) covers 45 scene categories, assembling in total 36 000 images of resolution 128x128 px. The data authors select various locations around the world, and follow China's landuse classification standard. This collection has 2-level label hierarchy with 6 super-categories: agricultural land, construction land and facilities, transportation and facilities, water and water conservancy facilities, woodland, and other lands. The preprocessed version of RSICB is created by resizing the images into 128x128 px using an anti-aliasing filter.  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/RSICB.png)

**Meta Album ID**: REM_SEN.RSICB  
**Meta Album URL**: [https://meta-album.github.io/datasets/RSICB.html](https://meta-album.github.io/datasets/RSICB.html)  
**Domain ID**: REM_SEN  
**Domain Name**: Remote Sensing  
**Dataset ID**: RSICB  
**Dataset Name**: RSICB  
**Short Description**: Remote sensing dataset  
**\# Classes**: 45  
**\# Images**: 1800  
**Keywords**: remote sensing, satellite image, aerial image, land cover  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: Open for research purposes  
**License (Meta-Album data release)**: CC BY-NC 4.0  
**License URL (Meta-Album data release)**: [https://creativecommons.org/licenses/by-nc/4.0/](https://creativecommons.org/licenses/by-nc/4.0/)  

**Source**: RSI-CB: A Large Scale Remote Sensing Image Classification Benchmark via Crowdsource Data  
**Source URL**: https://github.com/lehaifeng/RSI-CB  
  
**Original Author**: Haifeng Li, Xin Dou, Chao Tao, Zhixiang Hou, Jie Chen, Jian Peng, Min Deng, Ling Zhao  
**Original contact**: lihaifeng@csu.edu.cn  

**Meta Album author**: Phan Anh VU  
**Created Date**: 01 March 2022  
**Contact Name**: Ihsan Ullah  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
@article{li2020RSI-CB,
    title={RSI-CB: A Large-Scale Remote Sensing Image Classification Benchmark Using Crowdsourced Data},
    author={Li, Haifeng and Dou, Xin and Tao, Chao and Wu, Zhixiang and Chen, Jie and Peng, Jian and Deng, Min and Zhao, Ling},
    journal={Sensors},
    DOI = {doi.org/10.3390/s20061594},
    year={2020},
    volume = {20},
    number = {6},
    pages = {1594},
    type = {Journal Article}
}
```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Micro]](https://www.openml.org/d/44315)  [[Extended]](https://www.openml.org/d/44333)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44300) of an [OpenML dataset](https://www.openml.org/d/44300). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44300/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44300/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44300/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

